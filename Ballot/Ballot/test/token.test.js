const { expect, assert } = require("chai");
const { ethers } = require("hardhat");

function VotingFieldArrayToJSONWithoutMapping(personFieldArray) {
  const [
    stopped,
    rewardReceived,
    feeReceived,
    endTimeVoting,
    resultFee,
    highestVotingAmount,
    topCandidate,
  ] = personFieldArray;

  return {
    stopped,
    rewardReceived,
    feeReceived,
    endTimeVoting,
    resultFee,
    highestVotingAmount,
    topCandidate,
  };
}

describe("Ballot contract", function () {
  let ballot;
  let candidates;
  let accounts;
  const votingTime = 3 * 24 * 60 * 60;
  const votingFee = ethers.utils.parseEther("0.01");

  beforeEach(async function () {
    accounts = await ethers.getSigners();
    candidates = [
      accounts[10].address,
      accounts[11].address,
      accounts[12].address,
    ];
    const Ballot = await ethers.getContractFactory("Ballot", accounts[0]);
    ballot = await Ballot.deploy();
    await ballot.deployed();
  });

  describe("Deployment", function () {
    // `it` is another Mocha function. This is the one you use to define your
    // tests. It receives the test name, and a callback function.

    // If the callback function is async, Mocha will `await` it.
    it("Should set the right owner", async function () {
      // Expect receives a value, and wraps it in an Assertion object. These
      // objects have a lot of utility methods to assert values.

      // This test expects the owner variable stored in the contract to be equal
      // to our Signer's owner.
      expect(await ballot.owner()).to.equal(accounts[0].address);
    });
  });

  describe("Transactions", function () {
    describe("Before start voting", function () {
      it("Before start voting request isCandidate() should be error: No voting with this id", async () => {
        await expect(ballot.isCandidate(0, candidates[0])).to.be.revertedWith(
          "No voting with this id"
        );
      });

      it("Before start voting request isEndTimeForVoting() should be error: No voting with this id", async () => {
        await expect(ballot.isEndTimeForVoting(0)).to.be.revertedWith(
          "No voting with this id"
        );
      });

      it("Before start voting request getVoterVotingCandidate() should be error: No voting with this id", async () => {
        await expect(ballot.getVoterVotingCandidate(0)).to.be.revertedWith(
          "No voting with this id"
        );
      });

      it("Before start voting request getAmountTokensForCandidate() should be error: No voting with this id", async () => {
        await expect(
          ballot.getAmountTokensForCandidate(0, candidates[0])
        ).to.be.revertedWith("No voting with this id");
      });

      it("Before start voting request requestFee() should be error: No voting with this id", async () => {
        await expect(ballot.requestFee(0)).to.be.revertedWith(
          "No voting with this id"
        );
      });

      it("Before start voting request vote() should be error: No voting with this id", async () => {
        await expect(ballot.vote(0, candidates[0])).to.be.revertedWith(
          "No voting with this id"
        );
      });

      it("Before start voting request stopVoting() should be error: No voting with this id", async () => {
        await expect(ballot.stopVoting(0)).to.be.revertedWith(
          "No voting with this id"
        );
      });
    });
    describe("Start voting", function () {
      it("Start voting not owner will be revert: Ownable: caller is not the owner", async () => {
        await expect(
          ballot.connect(accounts[1]).startVoting(candidates)
        ).to.be.revertedWith("Ownable: caller is not the owner");
      });

      it("Start voting with empty canditates array and emit NewVoting event with votingId = 0 and list eq []]", async () => {
        await expect(ballot.startVoting([]))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, []);
      });

      it("Start voting with candidate[0] and emit NewVoting event with votingId = 0 and list eq candidate[0]", async () => {
        await expect(ballot.startVoting([candidates[0]]))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, [candidates[0]]);
      });

      it("Start voting with candidates and emit NewVoting event with votingId = 0 and list eq candidates", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);
      });

      it("Start 5 voting emit NewVoting votingId = 0 ... NewVoting votingId = 99", async () => {
        for (let i = 0; i < 5; i++) {
          await expect(ballot.startVoting(candidates))
            .to.emit(ballot, "NewVoting")
            .withArgs(i, candidates);
        }
      });

      it("Start voting with candidates and isCandidate() for every candidates true", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        for (const candidate of candidates) {
          const isCandidate = await ballot.isCandidate(0, candidate);
          expect(isCandidate).to.be.true;
        }
      });

      it("Start voting with candidates and isCandidate() for not candidates false", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        const isCandidate = await ballot.isCandidate(0, accounts[0].address);
        expect(isCandidate).to.be.false;
      });

      it("Start voting with candidates and isEndTimeForVoting() return false", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        const isEndTimeForVoting = await ballot.isEndTimeForVoting(0);
        expect(isEndTimeForVoting).to.be.false;
      });

      it("Start voting with candidates and isEndTimeForVoting() return true in 3 days", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        const isEndTimeForVoting = await ballot.isEndTimeForVoting(0);
        expect(isEndTimeForVoting).to.be.true;
      });

      it("Add all candidates for voting", async () => {
        await ballot.startVoting(candidates);

        const votingCandidates = await ballot.getCandidates(0);
        assert.deepEqual(votingCandidates, candidates);
      });
    });

    describe("Voting", function () {
      it("Voting for first candidate with fee 0.01 Eth will emit VoterVoted (votingId = 0, address = accounts[1])", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        await expect(
          ballot
            .connect(accounts[1])
            .vote(0, candidates[0], { value: votingFee })
        )
          .to.emit(ballot, "VoterVoted")
          .withArgs(0, accounts[1].address);
      });

      it("Voting for first candidate amountTokensForCandidate will return 0.01 eth and transfer from 0.01 eth to contract", async () => {
        await ballot.startVoting(candidates);
        const candidate = candidates[0];

        const amountTokensForCandidateBeforeVote =
          await ballot.getAmountTokensForCandidate(0, candidate);
        assert.deepEqual(
          amountTokensForCandidateBeforeVote,
          ethers.utils.parseEther("0.0")
        );

        await expect(() =>
          ballot.connect(accounts[1]).vote(0, candidate, { value: votingFee })
        ).to.changeEtherBalances(
          [accounts[1], ballot],
          [votingFee.mul(-1), votingFee]
        );

        const amountTokensForCandidateAfterVote =
          await ballot.getAmountTokensForCandidate(0, candidate);

        assert.deepEqual(amountTokensForCandidateAfterVote, votingFee);
      });

      it("Voting for first candidate with fee more then 0.01 Eth will be Error with: The voting fee must be exactly 0.01 Eth", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        await expect(
          ballot
            .connect(accounts[1])
            .vote(0, candidates[1], { value: votingFee + 1 })
        ).to.be.revertedWith("The voting fee must be exactly 0.01 Eth");
      });
      it("Voting for first candidate with fee less then 0.01 Eth will be Error with: The voting fee must be exactly 0.01 Eth", async () => {
        await expect(ballot.startVoting(candidates))
          .to.emit(ballot, "NewVoting")
          .withArgs(0, candidates);

        await expect(
          ballot.connect(accounts[1]).vote(0, candidates[1], {
            value: 0,
          })
        ).to.be.revertedWith("The voting fee must be exactly 0.01 Eth");
      });
      describe("VoterVotingCandidate ", function () {
        it("getVoterVotingCandidate after voting returns my candidate address", async () => {
          await ballot.startVoting(candidates);

          await ballot.connect(accounts[1]).vote(0, candidates[1], {
            value: votingFee,
          });

          const votingCandidateAddress = await ballot
            .connect(accounts[1])
            .getVoterVotingCandidate(0);

          assert.equal(votingCandidateAddress, candidates[1]);
        });
        it("getVoterVotingCandidate before voting returns address(0)", async () => {
          await ballot.startVoting(candidates);

          const votingCandidateAddress = await ballot
            .connect(accounts[1])
            .getVoterVotingCandidate(0);

          assert.equal(votingCandidateAddress, ethers.constants.AddressZero);
        });
      });

      describe("Can't vote if ", function () {
        it("Can't vote after end time ", async () => {
          await ballot.startVoting(candidates);

          await ethers.provider.send("evm_increaseTime", [votingTime]);
          await ethers.provider.send("evm_mine");

          await expect(
            ballot.connect(accounts[1]).vote(0, accounts[5].address, {
              value: votingFee,
            })
          ).to.be.revertedWith("Voting time is over");
        });

        it("Can't vote for candidate who there is not in candidate list ", async () => {
          await ballot.startVoting(candidates);

          await expect(
            ballot.connect(accounts[1]).vote(0, accounts[5].address, {
              value: votingFee,
            })
          ).to.be.revertedWith("No candidate with this address");
        });

        it("Can't vote twice for in same Voting", async () => {
          await ballot.startVoting(candidates);

          await ballot.connect(accounts[1]).vote(0, candidates[1], {
            value: votingFee,
          });

          await expect(
            ballot.connect(accounts[1]).vote(0, candidates[0], {
              value: votingFee,
            })
          ).to.be.revertedWith("You have already voted in this ballot.");
        });
      });
      describe("Top candidate ", function () {
        it("Top candidate before votes highestVotingAmount is 0 and topCandidate = 0x0", async () => {
          await ballot.startVoting(candidates);

          const voting = await ballot.votings(0);
          const votingJson = VotingFieldArrayToJSONWithoutMapping(voting);

          assert.equal(votingJson.highestVotingAmount.toNumber(), 0);
          assert.equal(votingJson.topCandidate, ethers.constants.AddressZero);
        });

        it("Top candidate after 1 vote highestVotingAmount is 0.01 eth and topCandidate = candidate[0]", async () => {
          await ballot.startVoting(candidates);

          await ballot.vote(0, candidates[0], { value: votingFee });

          const voting = await ballot.votings(0);
          const votingJson = VotingFieldArrayToJSONWithoutMapping(voting);

          assert.deepEqual(votingJson.highestVotingAmount, votingFee);
          assert.equal(votingJson.topCandidate, candidates[0]);
        });

        it("Top candidate swap after vote for the second candidate", async () => {
          await ballot.startVoting(candidates);

          //vote for 0 candidate
          await ballot.vote(0, candidates[0], { value: votingFee });
          //vote for 1 candidate
          await ballot
            .connect(accounts[1])
            .vote(0, candidates[1], { value: votingFee });

          let voting = await ballot.votings(0);
          let votingJson = VotingFieldArrayToJSONWithoutMapping(voting);

          assert.deepEqual(votingJson.highestVotingAmount, votingFee);
          assert.equal(votingJson.topCandidate, candidates[0]);

          //vote for 1 candidate and swap top candidate
          await ballot
            .connect(accounts[2])
            .vote(0, candidates[1], { value: votingFee });

          voting = await ballot.votings(0);
          votingJson = VotingFieldArrayToJSONWithoutMapping(voting);

          assert.deepEqual(votingJson.highestVotingAmount, votingFee.mul(2));
          assert.equal(votingJson.topCandidate, candidates[1]);
        });
      });
    });
    describe("Stop voting ", function () {
      it("Stop voting from account[1] after 3 days emit VotingFinished without winner (nobody voted)", async () => {
        await ballot.startVoting(candidates);

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        await expect(ballot.connect(accounts[1]).stopVoting(0))
          .to.emit(ballot, "VotingFinished")
          .withArgs(0, ethers.constants.AddressZero);
      });

      it("Stop voting from account[1] after 3 days emit VotingFinished with top candidate[1]", async () => {
        await ballot.startVoting(candidates);

        await ballot
          .connect(accounts[0])
          .vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        await expect(ballot.connect(accounts[1]).stopVoting(0))
          .to.emit(ballot, "VotingFinished")
          .withArgs(0, candidates[0]);
      });

      it("Stop voting twice will revert: voting is already stopped", async () => {
        await ballot.startVoting(candidates);

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        await ballot.connect(accounts[1]).stopVoting(0);
        await expect(
          ballot.connect(accounts[2]).stopVoting(0)
        ).to.be.revertedWith("voting is already stopped");
      });

      it("Stop voting from account[1] after 3 days voting.stopped = true and voting.rewardReceived = true", async () => {
        await ballot.startVoting(candidates);

        await ballot
          .connect(accounts[0])
          .vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        await ballot.connect(accounts[1]).stopVoting(0);

        let voting = await ballot.votings(0);
        let votingJson = VotingFieldArrayToJSONWithoutMapping(voting);

        assert.isTrue(votingJson.stopped);
        assert.isTrue(votingJson.rewardReceived);
      });

      it("Re-entrancy test Stop voting revert: voting is already stopped", async () => {
        await ballot.startVoting(candidates);

        await ballot
          .connect(accounts[0])
          .vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        ballot.connect(accounts[1]).stopVoting(0);
        await expect(
          ballot.connect(accounts[1]).stopVoting(0)
        ).to.be.revertedWith("voting is already stopped");

        let voting = await ballot.votings(0);
        let votingJson = VotingFieldArrayToJSONWithoutMapping(voting);
        assert.isTrue(votingJson.stopped);
        assert.isTrue(votingJson.rewardReceived);
      });

      it("Stop voting transfer rewards 0.09 Eth to winner and prepared resultFee 0.001 Eth", async () => {
        const resultReward = ethers.utils.parseEther("0.009");
        const resultFee = ethers.utils.parseEther("0.001");
        const candidate = accounts[10];
        await ballot.startVoting([candidate.address]);

        await ballot
          .connect(accounts[0])
          .vote(0, candidate.address, { value: votingFee });

        let voting = await ballot.votings(0);
        let votingJson = VotingFieldArrayToJSONWithoutMapping(voting);
        assert.equal(votingJson.resultFee.toNumber(), 0);

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        await expect(() =>
          ballot.connect(accounts[1]).stopVoting(0)
        ).to.changeEtherBalances(
          [ballot, candidate],
          [resultReward.mul(-1), resultReward]
        );

        voting = await ballot.votings(0);
        votingJson = VotingFieldArrayToJSONWithoutMapping(voting);
        assert.deepEqual(votingJson.resultFee, resultFee);
      });
    });

    describe("Request fee voting ", function () {
      it("Request fee after stop voting and owner received 0.001 Eth", async () => {
        const resultFee = ethers.utils.parseEther("0.001");
        await ballot.startVoting(candidates);

        await ballot.vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        ballot.connect(accounts[1]).stopVoting(0);

        await expect(() =>
          ballot.connect(accounts[0]).requestFee(0)
        ).to.changeEtherBalances(
          [ballot, accounts[0]],
          [resultFee.mul(-1), resultFee]
        );
      });

      it("Request fee before stopping voting and reward Received from winner revert: First you need to stop voting", async () => {
        await ballot.startVoting(candidates);

        await ballot.vote(0, candidates[0], { value: votingFee });

        await expect(
          ballot.connect(accounts[0]).requestFee(0)
        ).to.be.revertedWith("First you need to stop voting");
      });

      it("Re-entrance Request fee revert: You have already received a fee", async () => {
        await ballot.startVoting(candidates);

        await ballot.vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        ballot.connect(accounts[1]).stopVoting(0);

        ballot.connect(accounts[0]).requestFee(0);
        await expect(
          ballot.connect(accounts[0]).requestFee(0)
        ).to.be.revertedWith("You have already received a fee");
      });

      it("Request fee just later again request revert: You have already received a fee", async () => {
        await ballot.startVoting(candidates);

        await ballot.vote(0, candidates[0], { value: votingFee });

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        ballot.connect(accounts[1]).stopVoting(0);

        await ballot.connect(accounts[0]).requestFee(0);
        await expect(
          ballot.connect(accounts[0]).requestFee(0)
        ).to.be.revertedWith("You have already received a fee");
      });

      it("Request fee without winner in voting. Balance will not change", async () => {
        const resultFee = ethers.utils.parseEther("0.0");
        await ballot.startVoting(candidates);

        await ethers.provider.send("evm_increaseTime", [votingTime]);
        await ethers.provider.send("evm_mine");

        ballot.connect(accounts[1]).stopVoting(0);

        await expect(() =>
          ballot.connect(accounts[0]).requestFee(0)
        ).to.changeEtherBalances([ballot, accounts[0]], [resultFee, resultFee]);
      });
      //
    });
  });
});
