// SPDX-License-Identifier: MIT
pragma solidity >=0.7.0 <0.9.0;

import "./Ownable.sol";

contract Ballot is Ownable {

    event NewVoting(uint votingId, address[] withCandidates);
    event VotingFinished(uint votingId, address winner);
    event VoterVoted(uint votingId, address voter);

    uint32 constant votingTime = 3 days;
    uint32 constant public fee = 10;
    uint constant votingPrice = 0.01 ether;
    uint public countVotings;
    mapping (uint => Voting) public votings;

    struct Voting {
        bool stopped;
        bool rewardReceived;
        bool feeReceived;
        uint32 endTimeVoting;

        uint resultFee;
        uint highestVotingAmount;
        address topCandidate;

        address[] candidates;
        mapping (address => bool) isCandidate;

        mapping (address => address) votersVoting;
        mapping (address => uint) amountTokensForCandidate;
    }

    modifier hasVoting(uint _votingId) {
        require(_votingId < countVotings, "No voting with this id");
        _;
    }

    modifier containCandidate(uint _votingId, address _candidateAddress) {
        require(isCandidate(_votingId, _candidateAddress), "No candidate with this address");
        _;
    }

    modifier fixedCost(uint price) {
        require(msg.value == price, "The voting fee must be exactly 0.01 Eth");
        _;
    }

    modifier votingTimeNotEnded(uint _votingId) {
        require(!isEndTimeForVoting(_votingId), "Voting time is over");
        _;
    }

    modifier votingTimeEnded(uint _votingId) {
        require(isEndTimeForVoting(_votingId), "Voting time is not over");
        _;
    }

    function isCandidate(uint _votingId, address _candidateAddress) public view hasVoting(_votingId) returns(bool)  {
        return votings[_votingId].isCandidate[_candidateAddress];
    }

    function isEndTimeForVoting(uint _votingId) public view hasVoting(_votingId) returns(bool) {
        return block.timestamp >= votings[_votingId].endTimeVoting;
    }

    function getVoterVotingCandidate(uint _votingId) public view hasVoting(_votingId) returns (address) {
        //return address(0) if msgSender didn't vote
        return votings[_votingId].votersVoting[_msgSender()];
    }

    function getAmountTokensForCandidate(uint _votingId, address _candidateAddress) public view hasVoting(_votingId) returns (uint) {
        return votings[_votingId].amountTokensForCandidate[_candidateAddress];
    }

    function getCandidates(uint _votingId) public view hasVoting(_votingId) returns (address[] memory) {
        return votings[_votingId].candidates;
    }

    function startVoting(address[] calldata _candidates) external onlyOwner {
        Voting storage voting = votings[countVotings];
        voting.endTimeVoting = uint32(block.timestamp + votingTime);
        for (uint i = 0; i < _candidates.length; i++) {
            voting.candidates.push(_candidates[i]);
            voting.isCandidate[_candidates[i]] = true;
        }

        countVotings++;
        emit NewVoting((countVotings-1),  votings[countVotings-1].candidates);
    }

    function requestFee(uint _votingId) external onlyOwner hasVoting(_votingId) returns (bool){
        require(votings[_votingId].rewardReceived, "First you need to stop voting");
        require(!votings[_votingId].feeReceived, "You have already received a fee");

        uint amountFee = votings[_votingId].resultFee;
        if (amountFee > 0) {

            votings[_votingId].resultFee = 0;

            payable(_msgSender()).transfer(amountFee);
        }

        votings[_votingId].feeReceived = true;
        return true;
    }

    function vote(uint _votingId, address _cadidateAddress) payable external votingTimeNotEnded(_votingId) fixedCost(votingPrice) containCandidate(_votingId, _cadidateAddress) {
        Voting storage voting = votings[_votingId];
        require(voting.votersVoting[_msgSender()] == address(0), "You have already voted in this ballot.");

        voting.amountTokensForCandidate[_cadidateAddress] += msg.value;
        voting.votersVoting[_msgSender()] = _cadidateAddress;

        if(voting.amountTokensForCandidate[_cadidateAddress] > voting.highestVotingAmount) {
            voting.highestVotingAmount = voting.amountTokensForCandidate[_cadidateAddress];
            voting.topCandidate = _cadidateAddress;
        }

        emit VoterVoted(_votingId, _msgSender());
    }

    function stopVoting(uint _votingId) external votingTimeEnded(_votingId) returns (bool) {
        require(!votings[_votingId].stopped, "voting is already stopped");
        Voting storage voting = votings[_votingId];

        voting.stopped = true;

        uint resoultFee = 0;
        uint resoultReward = 0;

        //end if no one voted
        if(voting.topCandidate == address(0)) {
            voting.rewardReceived = true;
            emit VotingFinished(_votingId, voting.topCandidate);
            return true;
        }

        for (uint i = 0; i < voting.candidates.length; i++) {
            uint currentAmount = voting.amountTokensForCandidate[voting.candidates[i]];
            resoultReward += currentAmount;
        }

        resoultFee = (resoultReward * fee) / 100;
        resoultReward -= resoultFee;

        payable(voting.topCandidate).transfer(resoultReward);

        voting.resultFee = resoultFee;
        voting.rewardReceived = true;
        emit VotingFinished(_votingId, voting.topCandidate);
        
        return true;
    }
}