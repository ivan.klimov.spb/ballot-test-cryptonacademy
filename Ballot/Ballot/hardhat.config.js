/**
 * @type import('hardhat/config').HardhatUserConfig
 */

require("@nomiclabs/hardhat-waffle");
require("solidity-coverage");
require("@nomiclabs/hardhat-ganache");
require("./tasks/contract-interact");
const dotenv = require("dotenv");
dotenv.config({ path: __dirname + "/.env" });
const { API_URL, PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3 } = process.env;

module.exports = {
  solidity: "0.8.1",

  networks: {
    ganache: {
      url: "http://127.0.0.1:7545",
      //chainId: 1287, // 0x507 in hex,
      accounts: [PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3],
    },
    rinkeby: {
      url: API_URL,
      accounts: [PRIVATE_KEY, PRIVATE_KEY2, PRIVATE_KEY3],
    },
  },
};
