require("dotenv").config();
const API_URL = process.env.API_URL;
const { createAlchemyWeb3 } = require("@alch/alchemy-web3");
const web3 = createAlchemyWeb3(API_URL);
const contract = require("../contracts/artifacts/Ballot.json");

const contractAddress = "0x9BAb9841Dff18d72325BA9B73dF21AE137E50410";
const ballotContract = new web3.eth.Contract(contract.abi, contractAddress);

task("fee", "Print fee in contract").setAction(async () => {
  const fee = await ballotContract.methods.fee().call();
  console.log("The fee is: " + fee);
});

task(
  "countVotings",
  "Prints the number of votings since the start of the contract"
).setAction(async () => {
  const countVotings = await ballotContract.methods.countVotings().call();
  console.log("The countVotings is: " + countVotings);
});

task("voting", "Print voting info by id")
  .addParam("id", "voting id")
  .setAction(async (args) => {
    const voting = await ballotContract.methods.votings(args.id).call();
    console.log("stopped: " + voting[0]);
    console.log("rewardReceived: " + voting[1]);
    console.log("feeReceived: " + voting[2]);
    console.log("endTimeVoting: " + voting[3]);
    console.log("resultFee: " + voting[4]);
    console.log("highestVotingAmount: " + voting[5]);
    console.log("topCandidate: " + voting[6]);
  });

task("isCandidate", "Check candidate address on voting")
  .addParam("id", "voting id")
  .addParam("candidateAddress", "address candidate")
  .setAction(async (args) => {
    const isCandidate = await ballotContract.methods
      .isCandidate(args.id, args.candidateAddress)
      .call();
    console.log("isCandidate: " + isCandidate);
  });

task("isEndTimeForVoting", "Сheck the voting time is over")
  .addParam("id", "voting id")
  .setAction(async (args) => {
    const isEndTimeForVoting = await ballotContract.methods
      .isEndTimeForVoting(args.id)
      .call();
    console.log("isEndTimeForVoting: " + isEndTimeForVoting);
  });

task("getVoterVotingCandidate", "Get the candidate the voter voted for")
  .addParam("id", "voting id")
  .setAction(async (args) => {
    const getVoterVotingCandidate = await ballotContract.methods
      .getVoterVotingCandidate(args.id)
      .call();
    console.log("getVoterVotingCandidate: " + getVoterVotingCandidate);
  });

task("getAmountTokensForCandidate", "Amount of tokens voted for the candidate")
  .addParam("id", "voting id")
  .addParam("candidateAddress", "address candidate")
  .setAction(async (args) => {
    const getAmountTokensForCandidate = await ballotContract.methods
      .getAmountTokensForCandidate(args.id, args.candidateAddress)
      .call();
    console.log("getAmountTokensForCandidate: " + getAmountTokensForCandidate);
  });

task("getCandidates", "Get candidates in voting")
  .addParam("id", "voting id")
  .setAction(async (args) => {
    const getCandidates = await ballotContract.methods
      .getCandidates(args.id)
      .call();
    console.log("getCandidates: " + getCandidates);
  });

task("startVoting", "Start voting (only owner)")
  .addOptionalParam("from", "address from (it is id number from .env)")
  .addOptionalVariadicPositionalParam("candidates", "Candidate Address List")
  .setAction(async (args) => {
    let publicKey = getPublicKey(0);
    let privateKey = getPrivateKey(0);
    if (args.from) {
      publicKey = getPublicKey(args.from);
      privateKey = getPrivateKey(args.from);
    }

    let candidates = [];
    if (args.candidates) {
      candidates = args.candidates;
    }

    const ballot = new web3.eth.Contract(contract.abi, contractAddress, {
      from: publicKey,
      gasLimit: 300000000,
    });

    await signTransaction(
      () => ballot.methods.startVoting(candidates),
      publicKey,
      privateKey
    );
  });

task("vote", "Vote for candidate")
  .addParam("id", "voting id")
  .addParam("candidateAddress", "address candidate")
  .addOptionalParam("from", "address from (it is id number from .env)")
  .setAction(async (args) => {
    let publicKey = getPublicKey(0);
    let privateKey = getPrivateKey(0);
    if (args.from) {
      publicKey = getPublicKey(args.from);
      privateKey = getPrivateKey(args.from);
    }

    value = web3.utils.toWei("0.01", "ether");

    const ballot = new web3.eth.Contract(contract.abi, contractAddress, {
      from: publicKey,
      gasLimit: 300000000,
    });

    await signTransaction(
      () => ballot.methods.vote(args.id, args.candidateAddress),
      publicKey,
      privateKey,
      value
    );
  });

task("stopVoting", "Stop voting")
  .addParam("id", "voting id")
  .addOptionalParam("from", "address from (it is id number from .env)")
  .setAction(async (args) => {
    let publicKey = getPublicKey(0);
    let privateKey = getPrivateKey(0);
    if (args.from) {
      publicKey = getPublicKey(args.from);
      privateKey = getPrivateKey(args.from);
    }

    const ballot = new web3.eth.Contract(contract.abi, contractAddress, {
      from: publicKey,
      gasLimit: 300000000,
    });

    await signTransaction(
      () => ballot.methods.stopVoting(args.id),
      publicKey,
      privateKey
    );
  });

task("requestFee", "Request fee (only owner)")
  .addParam("id", "voting id")
  .addOptionalParam("from", "address from (it is id number from .env)")
  .setAction(async (args) => {
    let publicKey = getPublicKey(0);
    let privateKey = getPrivateKey(0);
    if (args.from) {
      publicKey = getPublicKey(args.from);
      privateKey = getPrivateKey(args.from);
    }

    const ballot = new web3.eth.Contract(contract.abi, contractAddress, {
      from: publicKey,
      gasLimit: 300000000,
    });

    await signTransaction(
      () => ballot.methods.requestFee(args.id),
      publicKey,
      privateKey
    );
  });

async function signTransaction(method, publicKey, privateKey, value = 0) {
  const nonce = await web3.eth.getTransactionCount(publicKey, "latest"); // get latest nonce
  let gasEstimate = await method().estimateGas({
    value: value,
  }); // estimate gas
  gasEstimate *= 3; //just for speed up
  const resultGas = parseInt(gasEstimate.toString());

  console.log(resultGas);

  // Create the transaction
  const tx = {
    from: publicKey,
    to: contractAddress,
    nonce: nonce,
    gas: resultGas,
    data: method().encodeABI(),
    value: value,
  };
  console.log(tx);

  const signedTx = await web3.eth.accounts.signTransaction(tx, privateKey);

  await web3.eth.sendSignedTransaction(
    signedTx.rawTransaction,
    function (error, hash) {
      if (!error) {
        console.log(
          "🎉 The hash of your transaction is: ",
          hash,
          "\n Check Alchemy's Mempool to view the status of your transaction!"
        );
      } else {
        console.log(
          "❗Something went wrong while submitting your transaction:",
          error
        );
      }
    }
  );
}

function getPublicKey(accountId) {
  switch (accountId) {
    case "0":
      return process.env.PUBLIC_KEY;
    case "1":
      return process.env.PUBLIC_KEY2;
    case "2":
      return process.env.PUBLIC_KEY3;
    default:
      return process.env.PUBLIC_KEY;
  }
}

function getPrivateKey(accountId) {
  switch (accountId) {
    case "0":
      return process.env.PRIVATE_KEY;
    case "1":
      return process.env.PRIVATE_KEY2;
    case "2":
      return process.env.PRIVATE_KEY3;
    default:
      return process.env.PRIVATE_KEY;
  }
}
