async function main() {
  const Ballot = await ethers.getContractFactory("Ballot");
  console.log("Deploying Ballot...");

  const ballot = await Ballot.deploy();

  await ballot.deployed();

  console.log("Ballot deployed to:", ballot.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
